all:
	mkdir -p bin
	gcc main.c -lssl -lcrypto -o bin/out

run:
	cd bin && ./out

clean:
	rm -rf bin
