# OpenSSL Certificate
Assignment of the 5th laboratory. This program posts a secure GET request to [https://fit.cvut.cz/student/odkazy](https://fit.cvut.cz/student/odkazy) and dumps the response into file `odkazy.html`. During the SSL connection, it also downloads the certificate and saves it into a file `fit_cert.pem`.

[EDUX](https://edux.fit.cvut.cz/courses/BI-BEZ/labs/05/start)

## Usage
You can build and run the application via makefile.

### Build
```bash
make
```

### Run
```bash
make run
```
Outputs files:
* `bin/odkazy.html`
* `bin/fit_cert.pem`

### Clean up build files
```
make clean
```

## Return codes
* `0`: Success.
* `1`: Internet address error.
* `2`: Cannot initialize socket.
* `3`: Cannot connect.
* `4`: SSL error.
* `5`: Web page file write error.
* `6`: Certificate file write error.
* `7`: Certificate download error.
